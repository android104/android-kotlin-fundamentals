package ru.pyshinskiy.messanger

import android.content.Context
import android.os.Bundle
import android.transition.Visibility
import android.transition.VisibilityPropagation
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var nicknameText: TextView
    private lateinit var nicknameEdit: EditText
    private lateinit var doneButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nicknameText = findViewById(R.id.nickname_text)
        nicknameEdit = findViewById(R.id.nickname_edit)
        doneButton = findViewById(R.id.done_button)
        doneButton.setOnClickListener {
            nicknameText.text = nicknameEdit.text.toString()
            nicknameEdit.visibility = View.GONE
            it.visibility = View.GONE
            nicknameText.visibility = View.VISIBLE
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
        }
        nicknameText.setOnClickListener {
            nicknameEdit.visibility = View.VISIBLE
            doneButton.visibility = View.VISIBLE
            it.visibility = View.GONE
            nicknameEdit.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(nicknameEdit, 0)
        }
    }
}